var Encore = require('@symfony/webpack-encore');
var ImageminPlugin = require('imagemin-webpack-plugin').default;
var CopyWebpackPlugin = require('copy-webpack-plugin');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    /*
     * Add JS
     */
    .addEntry('js/main', './assets/js/main.js')
    .addEntry('js/nav', './assets/js/nav.js')
    .addEntry('js/util', './assets/js/util.js')
    .addEntry('js/skel.min', './assets/js/skel.min.js')
    .addEntry('js/jquery.scrolly.min', './assets/js/jquery.scrolly.min.js')
    .addEntry('js/jquery.min', './assets/js/jquery.min.js')


    /*
     * Add CSS / SCSS
     */
    .addStyleEntry('css/main', './assets/sass/main.scss')
    .addStyleEntry('css/nav', './assets/sass/nav.scss')

    /*
     * Add images
     */
    .addPlugin(new CopyWebpackPlugin([{
        from: 'assets/images/',
        to: 'images/'
    }]))
    .addPlugin(new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i }))

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

    // uncomment for legacy applications that require $/jQuery as a global variable
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();

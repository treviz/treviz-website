/**
 * Created by Bastien on 21/12/2017.
 */

$(document).ready(function(){

    /*
     * Changes the navbar colors upon scrolling
     */
    var scroll_start = 0;
    var header_height = $('#home-header').height();
    if (!header_height) header_height =  $('#header').height();

    /*
     * On page load, look at the scroll status, and display the navbar appropriately
     */
    if ($(this).scrollTop() > header_height) {
        $('#navbar').css('background-color', '#f0f0f0');
        $('#navbar').css('color', 'black');
    }

    /*
     * When scrolling down, change the navbar display for it to display correctly
     */
    $(document).scroll(function() {
        scroll_start = $(this).scrollTop();
        if($('#navbar').width() > 736 ) {
            if (scroll_start > header_height) {
                $('#navbar').css('background-color', '#f0f0f0');
                $('#navbar').css('color', 'black');
            } else {
                $('#navbar').css('background-color', 'transparent');
                $('#navbar').css('color', 'white');
            }
        }
    });

    /*
     * When resizing the window, decide if the navbar should be displayed entirely or just as a menu.
     */
    $(window).resize(function() {

        if($('#navbar').width() > 736 ) { // If the page is displayed on a large display
            $('.nav_link').css('display', 'block');
            $('#navbar').css('flex-direction', 'row');
            $('#navbar').css('height', '60px');
            $('#navbar').css('justify-content', 'space-between');

            if ($(this).scrollTop() > header_height) {
                $('#navbar').css('background-color', '#f0f0f0');
                $('#navbar').css('color', 'black');
            } else {
                $('#navbar').css('background-color', 'transparent');
                $('#navbar').css('color', 'white');
            }
        } else { // if the page is displayed on a small display

            // If the navbar is displayed, keep it that way
            if ($('#navbar').height() == 250) {
                $('.nav_link').css('display', 'block');
                $('#navbar').css('flex-direction', 'column');
                $('#navbar').css('height', '250px');
                $('#navbar').css('justify-content', 'space-evenly');
                $('#navbar').css('background-color', '#f0f0f0');
                $('#navbar').css('color', 'black');
            } else {
                $('.nav_link').css('display', 'none');
                $('#menu_trigger').css('top', '42px');
                $('#navbar').css('background-color', 'transparent');
                $('#navbar').css('color', 'white');
                $('#navbar').css('height', '60px');
            }

        }
    });
});

$( "#menu_trigger" ).click(function() {

    if ($('#navbar').width() < 736) {
        if ($('#navbar').css('flex-direction') === 'column') {
            $('.nav_link').css('display', 'none'); // or none
            $('#navbar').css('flex-direction', 'row');
            $('#menu_trigger').css('top', '42px');
            $('#navbar').css('background-color', 'transparent');
            $('#navbar').css('color', 'white');
            $('#navbar').css('height', '60px');
        } else {
            $('.nav_link').css('display', 'block');
            $('#navbar').css('flex-direction', 'column');
            $('#navbar').css('height', '250px');
            $('#navbar').css('justify-content', 'space-evenly');
            $('#navbar').css('background-color', '#f0f0f0');
            $('#navbar').css('color', 'black');
            $('#menu_trigger').css('top', '292px');
        }
    }


});
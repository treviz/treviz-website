<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 20/12/2017
 * Time: 16:41
 */

namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route;

class HomeController
{

    /**
     * Displays Homepage.
     * @Route("/")
     * @param Environment $twig
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function index(Environment $twig) {
        return new Response($twig->render('pages/home.html.twig'));
    }

    /**
     * Displays contact page.
     * @Route("/contact")
     * @param Environment $twig
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function contact(Environment $twig) {
        return new Response ($twig->render('pages/contact.html.twig'));
    }

    /**
     * Displays about page.
     * @Route("/about")
     * @param Environment $twig
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function about(Environment $twig) {
        return new Response ($twig->render('pages/about.html.twig'));
    }

    /**
     * Displays about page.
     * @Route("/privacy-notice")
     * @param Environment $twig
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function notices(Environment $twig) {
        return new Response ($twig->render('pages/privacy-notice.html.twig'));
    }

    /**
     * Displays about page.
     * @Route("/terms-of-service")
     * @param Environment $twig
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function cgv(Environment $twig) {
        return new Response ($twig->render('pages/terms-of-service.html.twig'));
    }
}

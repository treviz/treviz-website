<?php

namespace App\Controller;

use App\Entity\Instance;
use App\Entity\InstanceCreationRequest;
use App\Form\InstanceCreationRequestType;
use App\Form\InstanceType;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class InstanceController extends Controller
{
    /**
     * Displays the various existing instances of Treviz, and a form to submit an existing one to review.
     * @Route("/instances")
     * @param Request $request
     * @param Environment $twig
     * @param RegistryInterface $doctrine
     * @param FormFactoryInterface $formFactory
     * @return Response
     */
    public function index(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $instances = $doctrine->getRepository(Instance::class)->findAll();
        $newInstance = new Instance();
        $form = $formFactory->createBuilder(InstanceType::class, $newInstance)->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine->getManager()->persist($newInstance);
            $doctrine->getManager()->flush();
        }

        return new Response($twig->render('instances/index.html.twig', array(
            'instances' => $instances,
            'form' => $form->createView()
        )));
    }

    /**
     * Asks for the creation of a new Treviz Instance.
     * @Route("/create-instance")
     * @param Request $request
     * @param Environment $twig
     * @param RegistryInterface $doctrine
     * @param FormFactoryInterface $formFactory
     * @return Response
     */
    public function create(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $instanceCreationRequest = new InstanceCreationRequest();
        $form = $formFactory->createBuilder(InstanceCreationRequestType::class, $instanceCreationRequest)->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine->getManager()->persist($instanceCreationRequest);
            $doctrine->getManager()->flush();
        }

        return new Response($twig->render('instances/create.html.twig', array(
            'form' => $form->createView()
        )));
    }
}
